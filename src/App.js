import React from 'react';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import Home from './components/pages/Home.js';
import About from './components/pages/About.js';
import Contact from './components/pages/Contact.js';
import Navbar from './components/layouts/Navbar.js';
import AddUser from './components/users/AddUser.js';
import EditUser from './components/users/EditUser.js';
import user from './components/users/user.js';
import PageNotFound from './components/pages/PageNotFound.js';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
function App() {
  return (
    <Router>
    <div className="App">
      <Navbar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/about" component={About} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/users/add" component={AddUser} />
        <Route exact path="/users/edit/:id" component={EditUser} />
        <Route exact path="/users/:id" component={user} />
        <Route component={PageNotFound} />
      </Switch>
    </div>
    </Router>
  ); 
}

export default App

